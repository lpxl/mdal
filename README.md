# mdal

Award medals

A simple command line application that allows you to award medals and rank entities.

## Installation

Only on cargo for now:
```sh
cargo install mdal
```

## Usage examples

#### For good managers:
```
mdal group set workers "Employees"
mdal entity set team1 "Best team"
mdal entity set joe "Joe" workers team1
mdal entity set mary "Mary" workers team1
mdal aspect set utried "Effort" star
mdal aspect set rust "Rust" medal
mdal aspect set nowhining "Work Ethics" medal
mdal award mary utried silver
mdal award mary rust gold
mdal award mary nowhining gold
mdal award joe utried bronze
mdal award joe utried silver
mdal award joe nowhining bronze
mdal entity rank team1
mdal profile mary
mdal profile joe
```

#### For film connoisseurs:

```
mdal group set films Films
mdal entity set nightdead "Night of the Living Dead" films
mdal entity set mean "Mean Girls" films
mdal aspect set cute "Cute" medal
mdal aspect set bloody "Bloody" heart
mdal award mean cute gold
mdal award mean bloody bronze
mdal award nightdead cute silver
mdal award nightdead bloody gold
mdal group rank films
```

You may also create other groups and parents afterwards:
```
mdal group set horror "Horror films"
mdal entity set dawndead "Dawn of the Dead" horror
mdal entity group nightdead horror
mdal group rank horror
....
mdal entity set dead "Dead series"
mdal entity set daydead "Day of the Dead" horror dead
mdal entity parent nightdead dead
mdal entity parent dawndead dead
mdal group rank horror
mdal entity rank dead
```

#### For systematic parents:
```
mdal group set kids "The Kids"
mdal entity set bob "Lil bob" kids
mdal entity set anne "Lil anne" kids
mdal aspect set dishes "Did Dishes" star
mdal aspect set room "Clean Room" star
mdal award anne room platinum
mdal award bob dishes silver
mdal group rank kids
```

#### For Kpop stanners:
```
mdal group set kpop "Kpop groups"
mdal group set idols "Idols"
mdal entity set lovelyz "Lovelyz"
mdal entity set omg "OhMyGirl"
mdal entity set mijoo "Mijoo" idols lovelyz
mdal entity set kei "Kei" idols lovelyz
....
mdal entity set yooa "Yooa" idols omg
mdal entity set arin "Arin" idols omg
....
mdal aspect set dancing "Dancing"
mdal aspect set singing "Singing"
mdal aspect set cute "Cuteness"
mdal award mijoo dancing gold
mdal award yooa dancing platinum
mdal award arin cute platinum
mdal award kei singing platinum
mdal award lovelyz dancing gold
mdal entity rank lovelyz
mdal entity rank omg
mdal entity rank kpop
mdal group rank idols
```

#### And for many others like weebs, sports fanatics, etc

If you make a mistake like setting the wrong group:
```
mdal entity group bob adults
```
Just repeat the same command again and the relationship will be removed.
This doesn't work for awards though.

## Todo

* Better reports
* Allow setting many groups/parents at once
* Optional description for medals
* Log commands and allow rebuilding the database

## License

Licensed under either of

* Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or https://www.apache.org/licenses/LICENSE-2.0)
* MIT license ([LICENSE-MIT](LICENSE-MIT) or https://opensource.org/licenses/MIT)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
license, shall be dual licensed as above, without any additional terms or
conditions.
