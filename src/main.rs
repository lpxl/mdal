// Copyright (c) 2018 lpxl <lpxl@protonmail.com>
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

#[macro_use]
extern crate log;
extern crate directories;
extern crate mdal;
extern crate simplelog;

use simplelog::*;
use std::env;
use std::fs;
use std::process;

fn main() {
    // Data directory setup
    let data_dir = if let Some(base_dirs) = directories::BaseDirs::new() {
        let dir = base_dirs.data_local_dir().join("mdal");
        fs::create_dir_all(&dir).expect("Could not create data directory!");
        dir
    } else {
        eprintln!("No valid home directory path could be retrieved!");
        match env::current_dir() {
            Ok(path) => path,
            Err(e) => {
                eprintln!("No access to current directory: {}", e);
                process::exit(2);
            }
        }
    };

    // Setup loggers
    let log_config = Config {
        time: None,
        ..Config::default()
    };
    CombinedLogger::init(vec![
        #[cfg(debug_assertions)]
        SimpleLogger::new(LevelFilter::Info, log_config),
        #[cfg(not(debug_assertions))]
        TermLogger::new(LevelFilter::Info, log_config)
            .expect("Failed terminal logger initialization!"),
        WriteLogger::new(
            LevelFilter::Trace,
            log_config,
            fs::File::create(data_dir.join("debug.log")).expect("Failed creating log file!"),
        ),
    ]).expect("Failed logger initialization!");

    // Run
    match mdal::run(&data_dir) {
        Ok(true) => process::exit(0),
        Ok(false) => process::exit(1),
        Err(e) => {
            error!("{}", e);
            process::exit(2)
        }
    }
}
